'use strict';

const utils = require('../utils');


/**
 * @api {get} /api/v1/configuration Get All Configurations
 * 
 * @apiSuccess {Array} returns all configurations
 *
 * @api {get} /api/v1/configuration:name Get configuration by name
 * 
 * @apiSuccess {object} returns configuration
 *
 * @api {post} /api/v1/configuration Create new configuration
 * 
 * @apiSuccess {string} Message 
 */

function configuration(req, res) {
  utils.checkToken(req.headers.authorization, req.app['users'], (err, user) => {
    if (err) {
      res.errorJson(400, 1321, 'Invalid Token', 'Invalid authorization token provided.');
    }
    if (user) {
      if (req.method === 'GET') {        
        if (req.params.name) {
          const item = req.app['configuration'].find((item) => {
            if (item.name === req.params['name']) {
              return item;
            }
          });

          if (item) {
            res.json(item);
          } else {
            res.errorJson(404, 2222, 'Invalid Configuration', 'Configuration Not Found');

          }
        } else {
          
          let configs = req.app['configuration'];
          // Sorting and pagination code.
          // Not the best implementation but achieved the simple goal.
          if(req.query['sort']) {
            const sortField = req.query['sort'];    
            configs = configs.sort(utils.sort(configs, sortField));
          }

          if(req.query['limit']) {
            const start = parseInt(req.query['start']) || 0;
            const limit = parseInt(req.query['limit']) + start;
            configs = utils.pagination(configs, start, limit);
          }
          res.json(configs);
        }
      }

      if (req.method === 'POST') {
        try {
          const json = req.body;
          const newConfig = {
            name: json['name'],
            hostname: json['hostname'],
            port: json['port'],
            username: json['username']
          };
          for (let key in newConfig) {
            if (!newConfig[key])
              throw new Error('Error creating configuration. ' + key + ' is required.');
          }
          req.app['configuration'].push(
            newConfig
          );
          res.json({ 'status': 'success', message: 'Configuration created' });
        } catch (e) {
          res.errorJson(400, 4122, 'Invalid Configuration', e.message);
        }
      }

      // Since I am using name as param in path, for this example I will only show updating
      // hostname, port, and username.
      if (req.method === 'PUT') {
        if (req.params) {
          const item = req.app['configuration'].find((item) => {
            if (item.name === req.params['name']) {
              return item;
            }
          });

          if (item) {
            const json = req.body;

            for (let key in json) {
              if (!json[key])
                throw new Error('Error creating configuration. ' + key + ' is required.');
            }
            item['hostname'] = json['hostname'];
            item['port'] = json['port'];
            item['username'] = json['username'];

            res.json({status: 'success', message: 'Configuration updated'});
          } else {
            res.errorJson(404, 2222, 'Item not found', 'Item does not exist. Cannot Update');

          }
        }
      }

      if (req.method === 'DELETE') {
        if (req.params) {
          const item = req.app['configuration'].find((item) => {
            if (item.name === req.params['name']) {
              return item;
            }
          });

          if (item) {
            const idx = req.app['configuration'].indexOf(item);
            req.app['configuration'].splice(idx, 1);
            res.json({status: 'success', message: 'Configuration deleted'});
          } else {
            res.errorJson(404, 2222, 'Item not found', 'Item does not exist. Cannot Delete');
          }
        }
      }
    }
  });
}
module.exports = {
  configuration: configuration
};
