'use strict';

const NodeServer = require('../lib/app');
const utils = require('./utils');
const mock = require('./mock');
const authRoutes = require('./routes/auth');
const configurationRoutes = require('./routes/configuration');


let loadFakeUserData = new Promise( (resolve, reject) => {
  let users = [];
  for( let i = 0; i < mock.users.length; i++) {

    let user = mock.users[i];
    utils.loadFakeUserData(user.username, user.password, (u) => {
      users.push(u); 

      if(users.length === mock.users.length -1) {
        resolve(users);
      }
    });
  }
});

let loadConfigurationData = new Promise( (resolve, reject) => {
  resolve(mock.configurations); 
});

function setupApp() {
  
  //Since we are loading more data and callbacks can get messy,
  //lets use a promise instead.
  Promise.all([loadFakeUserData, loadConfigurationData])
    .then( (resolve) => {
      const app = new NodeServer('localhost', 5000);

      //Create routes. 
      app.post('/api/v1/login', authRoutes.login);
      app.post('/api/v1/logout', authRoutes.logout);
      
      app.get('/api/v1/configuration/:name', configurationRoutes.configuration);
      app.post('/api/v1/configuration', configurationRoutes.configuration);
      app.put('/api/v1/configuration/:name', configurationRoutes.configuration);
      app.delete('/api/v1/configuration/:name', configurationRoutes.configuration);

      //Another way to load a route.
      //his route is used for returns of invalid endpoints.
      app.error(404, (req, res) => {
        res.errorJson(404, 2222, 'Invalid Endpoint', 'Endpoint Not Found' );
      });

      //Using this to just easily access my objects.
      app.set('users', resolve[0]);
      app.set('configuration', resolve[1]);
      app.run();  
    });
}

//Create App
setupApp();

