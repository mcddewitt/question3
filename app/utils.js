'use strict';

const crypto = require('crypto');

/**
* Simple hash password function
*/
function hashPassword(password, next) {
  crypto.pbkdf2(password, 'Some awesome salt', 100000, 512, 'sha512', (err, key) => {
    if (err) throw err;
    if (key) {
      next(key);
    }
  });
}

/**
* Checks the password provided for user
*/
function checkPassword(user, password, next) {
  hashPassword(password, (key) => {
    next(user.password.equals(key));
  });
}

/**
* Checks token passed with request
*/
function checkToken(token, users, next) {
  let user = users.find((u) => {
    return (u.token === token);
  });
  if(user){
    next(null, user);
  }
  if(!user) {
    next(new Error('Invalid token'));
  }
}

/**
* Load up some fake user data
*/
function loadFakeUserData(userName, password, next) {
  hashPassword(password, (key) => {
    let user = {
      userName: userName,
      password: key
    };   
    next(user);
  }); 
}

function sort(array, key) {
  return array.sort(function(a, b) {
    return a[key] > b[key] ? 1
      : a[key] < b[key] ? -1 : 0;
  });
}

function pagination(arr, start, limit) {
  return arr.slice(start, limit);
}


module.exports = {
  loadFakeUserData : loadFakeUserData,
  checkToken : checkToken,
  checkPassword: checkPassword,
  hashPassword: hashPassword,
  sort: sort,
  pagination: pagination,
};
