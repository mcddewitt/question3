'use strict';

const url = require('url');
const Response = require('./response');
const Request = require('./request');


/**
 * Handles the routing of resquest in application
 * 
 * @callback route function if found, else 404 Not Found
 */
module.exports = class RequestHandler {
  constructor(ctx) {
    this.routes = [];
    this.findRoute = this.findRoute.bind(this);
    this.errorRoute = [];
    //Simple way to pass data around the app.  In this case just my users.
    this.appContext = ctx;
  }

  /**
  * Adds route to routes array.  Queried by findRoute function.
  * not the best implementation but works for this example.
  */
  addRoute(routePath, method, next) {
    let params = [];
    let path = routePath;
    let pathTmp = routePath;
    let idx = pathTmp.indexOf(':');
    path = idx == -1 ? path : path.slice(0, idx);
    path = path.replace(/\/$/, '');

    while(pathTmp.length > 0){
      const slashIdx = pathTmp.indexOf('/') ? pathTmp.indexOf('/') >= 0 : pathTmp.length;
      let param = pathTmp.slice(idx+1, slashIdx);
      params.push(param);
      pathTmp = pathTmp.slice(slashIdx, pathTmp.length);
      idx = pathTmp.indexOf(':');
    }

    this.routes.push({
      path: path,
      method: method,
      params: params,
      next: next
    });
  }

  /**
  * Adds route to errorRoute array. If handleRequest doesn't find a route 
  * for request path, then it checks errorRoute for 404.
  */
  addErrorHandler(code, next) {
    this.errorRoute.push({code: code, next: next});
  }

  /**
  * Queries route array for route matching path and method.  
  * @returns route
  */
  findRoute(path, method) {
    for (let route of this.routes) {
      if(route.params.length > 0) {
        let arr = path.split('/');
        let paramValues = arr.splice(-route.params.length, arr.length);
        //Reconstruct route for use in check
        let rt = arr.join('/');

        if (route.path === rt && route.method === method) {
          let params = {};
          for(let i = 0; i < route.params.length; i++){
            params[route.params[i]] = paramValues[i];
          }
          return {route: route, params: params};
        }
      }
      if (route.path === path && route.method === method) {
        return {route: route, params: {}};
      }
    }
  }

  /**
  * Handles Request by checking routes for request route and matching it 
  * to route callback.  If none return 404
  */
  handleRequest(req, res) {
    const path = url.parse(req.url).pathname;
    const route = this.findRoute(path, req.method);
    
    //Working to make classes of request and response easier to use.
    //Not the best implementation and needs some work.
    let request = new Request(req, this.appContext);
    let response = new Response(res);
    
    if (route) {
      request.parseBody(() => {
        request.params = route.params;
        route.route.next(request, response);
      });
    } 
    else {
      for (let errRoute of this.errorRoute) {
        if (errRoute.code === 404 ) {
          return errRoute.next(request,response);
        }
      }
    }

  }
};